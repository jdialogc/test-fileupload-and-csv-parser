package com.jdialogc.tests.csv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestCsvParserApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestCsvParserApplication.class, args);
	}
}
