/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jdialogc.tests.csv.service;

import com.jdialogc.tests.csv.entity.Opinion;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Cornelius M
 */
public interface ResourceService {
    
    public void store(MultipartFile resource) throws FileNotFoundException, IOException;
    
    public <T> List<T> convertCsv(Class<T> entity, MultipartFile file) throws IOException;

}
