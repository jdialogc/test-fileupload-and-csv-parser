/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jdialogc.tests.csv.service.template;

//import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.jdialogc.tests.csv.service.ResourceService;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Cornelius M
 */
@Service
public class ResourceServiceTemplate implements ResourceService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public void store(MultipartFile resource)  throws FileNotFoundException, IOException{
        log.info("Processing file storage for file " + resource.getOriginalFilename());
//        Resource imageSource = new ClassPathResource("static/img/icon.png");
//        imageSource.
        String fileUrl = "upload/" + resource.getOriginalFilename();
        BufferedOutputStream stream
                = new BufferedOutputStream(new FileOutputStream(new File(fileUrl)));
        stream.write(resource.getBytes());
        stream.close();
    }

    @Override
    public <T> List<T> convertCsv(Class<T> entity, MultipartFile file) throws IOException {
        CsvMapper mapper = new CsvMapper();
        CsvSchema bootstrapSchema = CsvSchema.emptySchema().withHeader();
        MappingIterator<T> readValues = mapper.readerFor(entity).with(bootstrapSchema).readValues(file.getInputStream());

        return readValues.readAll();
//        CsvSchema.emptySchema().withHeader();
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//        mapper.readValue(new File(), List<T.>);
    }

}
