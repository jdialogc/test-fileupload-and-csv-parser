/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jdialogc.tests.csv.controller;

import com.jdialogc.tests.csv.entity.Opinion;
import com.jdialogc.tests.csv.service.ResourceService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Cornelius M
 */
@Controller
public class MainController {
    
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final ResourceService resourceService;
    
    public MainController(ResourceService resourceService){
        this.resourceService = resourceService;
    }
    
    @RequestMapping("/")
    public String index(){
        return "index";
    }
    
    @RequestMapping(value  = "/process-csv", method = RequestMethod.POST)
    public String processUpload(@RequestParam("csv") MultipartFile file, Model model){
        log.info("Processing file ({}) upload...", file.getOriginalFilename());
        try {
            ArrayList<Opinion> entities = (ArrayList<Opinion>)resourceService.convertCsv(Opinion.class, file);
            log.info("Managed to convert to {} entities", entities);
            resourceService.store(file);
        } catch (IOException ex) {
            log.error("Failed to parse file to entities", ex);
            model.addAttribute("message", "File upload error details: " + ex.getLocalizedMessage());
            return "index";
        }
        
        model.addAttribute("message", "File upload successfully");
        return "index";
    }

}
